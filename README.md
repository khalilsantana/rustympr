**Rusty MPR** ![Pipeline master](https://gitlab.com/CactiLover/rustympr/badges/master/pipeline.svg)



This project is a small library and demo application which performs multiplanar reconstruction based on image slices (volumes)

**Example**

`$ rustympr --dataset ../dataset/jpg --horizontal 300`

This will output an image with the default name horizontal.png in the current working directory, like this image:

![](demo-output.png "Horizontal Slice 300")

**Options**

```
$ rustympr --help
rustympr 0.1.0
Khalil Santana <d43m0n5@protonmail.com>
A demo application which performs multiplanar reconstruction based on image slices (volumes)

USAGE:
    rustympr [OPTIONS] --dataset <DATASET_PATH>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --dataset <DATASET_PATH>    The image slices dataset
        --horizontal <H_SLICE>      The horizontal slice you wish to generate from the image slices
    -o, --output <OUTPUT_PATH>      Where to place the output image(s). Defaults to current directory
        --vertical <V_SLICE>        The vertical slice you wish to generate from the image slices
```

**Building**

```shell
git clone https://gitlab.com/CactiLover/rustympr.git
cd rustympr/
cargo build --release
```
