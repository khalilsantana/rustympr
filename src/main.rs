use clap::*;

use std::path::Path;

mod mpr;

fn main() {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about("A demo application which performs multiplanar reconstruction based on image slices (volumes)")
        .arg(
            Arg::with_name("DATASET_PATH")
                .long("dataset")
                .short("d")
                .help("The image slices dataset")
                .takes_value(true)
                .required(true))
        .arg(
            Arg::with_name("OUTPUT_PATH")
                .long("output")
                .short("o")
                .help("Where to place the output image(s). Defaults to current directory")
                .takes_value(true))
        .arg(
            Arg::with_name("V_SLICE")
                .long("vertical")
                .help("The vertical slice you wish to generate from the image slices")
                .takes_value(true))
        .arg(
            Arg::with_name("H_SLICE")
                .long("horizontal")
                .help("The horizontal slice you wish to generate from the image slices")
                .takes_value(true))
        .get_matches();
    let v_slice_str = matches.value_of("V_SLICE");
    let h_slice_str = matches.value_of("H_SLICE");
    let dataset_path = Path::new(matches.value_of("DATASET_PATH").unwrap());
    if !dataset_path.exists() {
        eprintln!("{} doesn't exist, aborting", dataset_path.display());
        std::process::exit(exitcode::IOERR);
    }
    let output_path = Path::new(matches.value_of("OUTPUT_PATH").unwrap_or("./"));
    let mut my_mpr = mpr::Mpr::new(dataset_path);
    eprintln!(
        "Finished loading {} images of {}px",
        my_mpr.load_data(),
        my_mpr.get_size()
    );
    if v_slice_str.is_some() {
        let v_slice = value_t!(matches, "V_SLICE", u32).unwrap_or_else(|e| e.exit());
        my_mpr
            .get_vertical_slice(v_slice)
            .unwrap()
            .save(output_path.clone().join("vertical.png"))
            .expect("Couldn't write file");
    }
    if h_slice_str.is_some() {
        let h_slice = value_t!(matches, "H_SLICE", u32).unwrap_or_else(|e| e.exit());
        my_mpr
            .get_horizontal_slice(h_slice)
            .unwrap()
            .save(output_path.join("horizontal.png"))
            .expect("Couldn't write file");
    }
}
