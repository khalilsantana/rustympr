use std::fs;
use std::io::{self, Write};
use std::path::Path;

use image::{DynamicImage, GenericImage, GenericImageView};

pub struct Mpr<'a> {
    size: usize,
    dataset: Option<Vec<DynamicImage>>,
    dataset_path: &'a Path,
}

impl Mpr<'_> {
    pub fn new(dataset_path: &Path) -> Mpr {
        Mpr {
            size: 0,
            dataset: None,
            dataset_path,
        }
    }
    pub fn load_data(&mut self) -> usize {
        self.dataset = Option::from(Vec::new());
        let paths = fs::read_dir(&self.dataset_path).unwrap();
        let mut files = paths
            .filter_map(|v| v.ok())
            .map(|v| v.path())
            .filter(|v| v.is_file())
            .collect::<Vec<_>>();
        files.sort();
        for f in files {
            eprint!("\rLoading {}", f.display());
            io::stdout().flush().unwrap();
            let img = image::open(f).unwrap();
            self.dataset.as_mut().unwrap().push(img);
        }
        eprintln!();
        // Assume squace image
        self.size = self
            .dataset
            .as_ref()
            .unwrap()
            .get(0)
            .unwrap()
            .dimensions()
            .0 as usize;
        return self.dataset.as_ref().unwrap().len();
    }
    pub fn get_vertical_slice(&self, slice: u32) -> Option<DynamicImage> {
        if self.dataset.is_some() {
            let mut output = DynamicImage::new_rgb8(
                self.size as u32,
                self.dataset.as_ref().unwrap().len() as u32,
            );
            for i in 0..self.dataset.as_ref().unwrap().len() {
                for j in 0..self.size {
                    let pixel = self
                        .dataset
                        .as_ref()
                        .unwrap()
                        .get(i)
                        .unwrap()
                        .get_pixel(slice, j as u32);
                    output.put_pixel(j as u32, i as u32, pixel);
                }
            }
            return Some(output);
        } else {
            return None;
        }
    }
    pub fn get_horizontal_slice(&self, slice: u32) -> Option<DynamicImage> {
        if self.dataset.is_some() {
            let mut output = DynamicImage::new_rgb8(
                self.size as u32,
                self.dataset.as_ref().unwrap().len() as u32,
            );
            for i in 0..self.dataset.as_ref().unwrap().len() {
                for j in 0..self.size {
                    let pixel = self
                        .dataset
                        .as_ref()
                        .unwrap()
                        .get(i)
                        .unwrap()
                        .get_pixel(j as u32, slice);
                    output.put_pixel(j as u32, i as u32, pixel);
                }
            }
            return Some(output);
        } else {
            return None;
        }
    }
    pub fn get_size(&self) -> &usize {
        &self.size
    }
}
